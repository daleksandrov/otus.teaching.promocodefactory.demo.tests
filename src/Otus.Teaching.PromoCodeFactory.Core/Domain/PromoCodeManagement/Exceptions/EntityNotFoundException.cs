﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement.Exceptions
{
    public class EntityNotFoundException
        : Exception
    {
        public EntityNotFoundException(string message)
            : base(message)
        {
            
        }
    }
}